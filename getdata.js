/**
 * Created by Muzamil on 25-Oct-15.
 */

var getdata=angular.module('getdata',[]);

    getdata.service('venue',function venue ($http,$q){
        //storing reference of service into variable
        var venue=this;
        // create empty array to store venue lists
        venue.venueList={};

        venue.getAllVenues=function(query){


        var defer=$q.defer();
        $http.get(query)
            .success(function(res){
                console.log(query);
                venue.venueList=res;
                defer.resolve(res);
                console.log(query);
            })
            .error(function(error,status){
                defer.reject(error);
            })

        return defer.promise;

    }
})

