/**
 * Created by Muzamil on 29-Oct-15.
 */



describe('Service:GetDataService', function() {
    beforeEach(module('getdata'));

    var venue;
    var $httpBackend;
    var queryString='Oulu';
    var prepaidQuery="https://api.foursquare.com/v2/venues/search?client_id=fakeClient&client_secret=fakeSecret&v=fakeVersion&ll=fakeLocation&query="+queryString;
    var response={"meta":{"code":200,"requestId":"563306af498e9f42d04867e9"},"response":{"venues":[{"id":"4dbfa38e6a23e5a549d9f4ae","name":"Oulun yliopisto \/ University of Oulu","contact":{"phone":"+358294480000","formattedPhone":"+358 29 4480000"},"location":{"address":"Pentti Kaiteran Katu 1","lat":65.05905002747056,"lng":25.467893681045236,"distance":308,"postalCode":"90570","cc":"FI","city":"Oulu","country":"Finland","formattedAddress":["Pentti Kaiteran Katu 1","90570 Oulu","Finland"]},"categories":[{"id":"4bf58dd8d48988d1ae941735","name":"University","pluralName":"Universities","shortName":"University","icon":{"prefix":"https:\/\/ss3.4sqi.net\/img\/categories_v2\/education\/default_","suffix":".png"},"primary":true}],"verified":true,"stats":{"checkinsCount":15966,"usersCount":1179,"tipCount":11},"url":"http:\/\/www.oulu.fi","specials":{"count":0,"items":[]},"venuePage":{"id":"42745864"},"storeId":"","hereNow":{"count":0,"summary":"Nobody here","groups":[]},"referralId":"v-1446184623","venueChains":[]}]}};

    beforeEach(inject(function(_$httpBackend_,_venue_){
        venue=_venue_;
        $httpBackend=_$httpBackend_;
        venue.getAllVenues(prepaidQuery);

        $httpBackend.when('GET',prepaidQuery).respond(200,response);


    }));

    afterEach(function(){

        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    })

    it('should get response back from server into venue list',function(){
        $httpBackend.expectGET(prepaidQuery);
        $httpBackend.flush();
        expect(venue.venueList).not.toBeUndefined();
    })

    it('should return an array list of venues in case of success code =200 ',function(){
        $httpBackend.expectGET(prepaidQuery);
        $httpBackend.flush();
        if(venue.venueList.meta.code==200){
            expect(venue.venueList.response.venues).not.toBeUndefined();

        }

    })
    it('should return should return venues which name contains query string from user input ',function(){
        $httpBackend.expectGET(prepaidQuery);
        $httpBackend.flush();
        if(venue.venueList.meta.code==200){
            expect(venue.venueList.response.venues[0].name).toContain("Oulu");

        }
    });
    it('should return should return venues address ',function(){
        $httpBackend.expectGET(prepaidQuery);
        $httpBackend.flush();
        if(venue.venueList.meta.code==200){
            expect(venue.venueList.response.venues[0].location.address).toContain("Pentti Kaiteran Katu 1");
        }
    });

    it('should return should return venues distance ',function(){
        $httpBackend.expectGET(prepaidQuery);
        $httpBackend.flush();
        if(venue.venueList.meta.code==200){
            expect(venue.venueList.response.venues[0].location.distance).toEqual(308);
        }
    });

});
