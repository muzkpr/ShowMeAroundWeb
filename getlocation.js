/**
 * Created by Muzamil on 25-Oct-15.
 */


var getlocation=angular.module('getlocation',[])

getlocation.service("location",function location ($q) {

    //storing reference of service into variable
    var location=this;
    location.position={};
    //get location is an async call to handle this, we use defer
    var defer=$q.defer();

    location.getLocation=function(){
        // check if browser supports geolocation
        if (navigator.geolocation) {
            console.log("inside get location before sending request");

            //request to get geolocation
            navigator.geolocation.getCurrentPosition(function(position) {
                //to resolve async call
                console.log("inside get location"+position);

                defer.resolve(position);



            }, function(error) {
                defer.reject(error);

            });

            return defer.promise;


        } else {
            console.log("failed to get location");


            this.showError();
        // Fallback for no geolocation
        alert( "Geolocation is not supported by this browser.")
        }


    }

    this.showError=function(error) {
        switch(error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.");
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.");
                break;
        }
    }

});

