
    var app=angular.module('ShowMeAround', ['getdata','getlocation','ngCookies']);



        app.controller("VenueController", ['$scope', 'venue','location','$cookies',function ($scope, venue, location,$cookies) {

            var base="https://api.foursquare.com/v2/venues/search?";
            var client_id="";
            var client_secret="";
            var version="20130815";
            $scope.query = "";
            var latitude="0";
            var longitude="0";
            var accuracy="0";

            $scope.init = function () {
                // Check if location data is not available then try to get location data
                if(latitude=="0"){
                    //check if location data is available in cookies
                    if($cookies.get("posLat")) {
                        latitude  = $cookies.get("posLat");
                        longitude = $cookies.get("posLon");
                        accuracy  = $cookies.get("posAccuracy");
                        console.log("get from cookies"+latitude);

                    }
                    //else call the getLocation method
                    else{
                        $scope.getLocation();
                    }
                }
                //if location data is available then call to foursquare server.
                else{
                    var prepaidQuery=$scope.buildQuery(latitude,longitude,$scope.query);
                    $scope.getAllVenues(prepaidQuery);

                }

            };
            // method to get current geo location, we store geolocation for 2 min in cookies in case user change his location in after 2 minutes
            $scope.getLocation=function(){
                location.getLocation()
                .then(function(position){
                        //console.log("position"+position);

                        // Get the location data
                        latitude = position.coords.latitude;
                        longitude = position.coords.longitude;
                        accuracy = position.coords.accuracy;

                        // set expire time to 2 min for cookies
                        var now = new Date();
                        var time = now.getTime();
                        time += 2*60 * 1000;
                        now.setTime(time);
                        var expireTime=now.toUTCString();
                        // as getLocation is Async call so we are store location data in cookies for further use.
                        $cookies.put("posLat", latitude,{expires: expireTime});
                        $cookies.put("posLon", longitude,{expires: expireTime});
                        $cookies.put("posAccuracy", accuracy,{expires: expireTime});
                        console.log("position"+position);
                    })
            };

            // build get request for foursquare
            $scope.buildQuery = function (latitude, longitude, query) {
                return base+"client_id="+client_id+"&client_secret="+client_secret+"&v="+version+"&ll="+latitude+","+longitude+"&query="+query;

            };
            $scope.getAllVenues=function(query){
                console.log('get all venues get called');
                // get all venue in result of the query
                venue.getAllVenues(query)
                    .then(function(res){
                        // when response come back from server then store reference of response in $scope.venues for two way data binding
                        $scope.venues=venue.venueList.response.venues;
                        console.log($scope.venues);

                    },function(err){

                    })
            };

            $scope.init();

        }]);












		
	
