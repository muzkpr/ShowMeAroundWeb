/**
 * Created by Muzamil on 26-Oct-15.
 */


describe('VenueController', function() {
    beforeEach(module('ShowMeAround'));

    var VenueController;
    var scope;
    var getLocationService;
    var q;
    var deferred;
    var cookies;
    var latitude;
    var accuracy;
    var longitude;
    var base="https://api.foursquare.com/v2/venues/search?";
    var client_id="";
    var client_secret="";
    var version="20130815";
    var query = "";
    var prepaidQuery;
    var position={"coords": {"latitude": 65.14,"longitude": 25.65}};


    beforeEach(function() {
        module('getlocation');
    });



    beforeEach(function(){
        getLocationService={

            getLocation:function()
            {
                deferred=q.defer();
                deferred.resolve(position);

                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                accuracy = position.coords.accuracy;
                cookies.posLat = latitude;
                cookies.posLon = longitude;
                cookies.posAccuracy = longitude;
                return deferred.promise;
            }
        }
    });




    beforeEach(inject(function ($controller,$rootScope,$q,$cookies) {
        q=$q;
        cookies=$cookies;
        scope=$rootScope.$new();
        VenueController = $controller('VenueController',{
            $scope:scope,
            $q:q,
            location:getLocationService,
        });

        deffered=$q.defer();

        expect(VenueController).toBeDefined();
        locationServiceSpy=spyOn(scope,'getLocation').and.returnValue(deferred.promise);


    }));

    describe('get current location from location service',function(){
        //
        it('should call get location service '  ,function(){
            scope.init();
            expect(locationServiceSpy).toHaveBeenCalled();
        } );

        it('should call get location and get latitude back'  ,function(){

            expect(latitude).toBe(65.14);
        } );

        it('should call get location and get longitude back'  ,function(){
            expect(longitude).toBe(25.65);
        } );

        it('should store and retrieve latitude from cookies'   ,function(){
            expect(cookies.posLat).toBe(65.14);
        } );

        it('should store and retrieve longitude from cookies'  ,function(){
            expect(cookies.posLon).toBe(25.65);
        } );
    })

    it('should return prepaid query query',function(){
        prepaidQuery=scope.buildQuery(latitude,longitude,query);
        expect(prepaidQuery).toBe(base+'client_id='+client_id+'&client_secret='+client_secret+'&v='+version+'&ll='+latitude+','+longitude+'&query='+query);
    })

});
